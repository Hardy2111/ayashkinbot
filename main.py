from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update, ReplyKeyboardMarkup, KeyboardButton, \
    WebAppInfo, LoginUrl, ReplyKeyboardRemove
from telegram.ext import CommandHandler, CallbackContext, MessageHandler, filters, CallbackQueryHandler, \
    ApplicationBuilder
import logging
import sqlite3
import traceback
import telegram.error
from telegram import InputMediaPhoto
import sqlite3
import logging
import traceback
from datetime import datetime, timedelta
from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import declarative_base, sessionmaker

Base = declarative_base()
engine = create_engine('sqlite:///Data.db', echo=True)
Session = sessionmaker(bind=engine)
session = Session()


class User(Base):
    __tablename__ = 'users'
    user_id = Column(Integer, primary_key=True)
    user_name = Column(String)
    last_message = Column(Integer)
    phone = Column(Integer)
    event = Column(String)

    def __str__(self):
        return self.user_id


TOKEN = "6033715715:AAEcDo6HW6NkrTb8araGXYJwJ5HGkfCHuDE"
logging.basicConfig(filename='logfile.log', format='%(asctime)s %(levelname)-8s %(message)s', level=logging.ERROR,
                    datefmt='%Y-%m-%d %H:%M:%S')

CHANNEL = "t.me/ayashkin_content"

FILE = "BQACAgIAAxkBAANmZGR5yqETvLj3y0LpPfDt2_sz2KAAAtMrAAKm7ilLKNBVMgABfpRuLwQ"

photos = {
    1: {"photo": 0,
        "text": '⚡️Реферальная воронка, которая увеличила лидов с 9.000 до 29.000\n\nКлиент: <a href="https://instagram.com/khodzhaev_azam">Азам Ходжаев</a>\n\nНиша: инфобизнес, NFT\n\nМы с командой создали бота с геймификацией, который увеличивал количество людей в воронке за счёт рекомендательного маркетинга'},
    2: {"photo": 0,
        "text": '⚡️Арбитражный сканер с ежемесячной подпиской\n\nНиша: арбитраж\n\nПартнёры: <a href="https://instagram.com/s_romanovich">Сергей Романович</a>, <a href="https://instagram.com/ivanenko.bz">Алексей Иваненко</a>\n\nМы создали полноценный сервис Spread Bot, который позволяет арбитражникам быстро находить спреды. Сейчас он приносит около 1 млн₽/мес. пассивно\n\nЭтот проект был самым сложным в реализации'},
    3: {"photo": 0,
        "text": '⚡️Бот для парсинга аудитории\n\nНиша: сервисы для рассылок\n\nМы упростили задачу для тех, кому нужно быстро собрать контакты из групп Телеграма'},
    4: {"photo": 0,
        "text": '⚡️Бот для уведомлений о мероприятиях и бизнес-тиндером\n\nКлиент: <a href="https://instagram.com/faldin.a.e">Команда SAYYES</a>\n\nНиша: бизнес-клубы\n\nПеред нами стояла задача – сделать так, чтобы участники бота рандомно смогли общаться друг с другом, а также – собирать контакты людей в общую базу.\n\nОсобенность этого проекта в том, что работали с минимальным ТЗ и через призму своего опыта докручивали проект'},
    5: {"photo": 0,
        "text": '⚡️Реферальная воронка с геймификацией\n\nКлиент: <a href="https://instagram.com/raze_iam">Андрей Белоусов</a>\n\nНиша: инфобизнес, WB\n\nЗадача состояла в том, чтобы вовлечь людей на этапе бесплатника перед вебинаром и увеличить количество лидов без вложений в рекламу.'},
}

for i in photos:
    photos[i]["photo"] = "AgACAgIAAxkBAAMLYuAf1HMGpRC6UMGXfwKkpDL2vKoAAg_CMRtClAFLHm6h14YtNRcBAAMCAANzAAMpBA"


async def add_person(message):
    try:
        user = User(user_id=message.chat_id, user_name=message.chat.username, last_message=0, phone=0, event="")
        session.add(user)
        session.commit()
    except Exception:
        logging.error(traceback.format_exc())

subs_markup = InlineKeyboardMarkup(
            inline_keyboard=[[InlineKeyboardButton(text='Проверить подписку 🔄', callback_data="channel")]])

async def send_markup_with_text(message, markup, text):
    await message.reply_text(text, reply_markup=markup)

async def start_message(update: Update, context: CallbackContext):
    user = session.query(User).filter_by(user_id=update.message.chat_id).first()
    print(update.to_json())
    if user == None:
        await add_person(update.message)
        # markup = ReplyKeyboardMarkup(
        #     keyboard=[[KeyboardButton(text="🗣обо мне"), KeyboardButton(text="🌟кейсы")],
        #               [KeyboardButton(text='✍🏻обсудить проект')]],
        #     resize_keyboard=True)
        await send_markup_with_text(update.message, subs_markup, '🥳Добро пожаловать на мой бесплатный интенсив, где мы разберем пошаговый план выхода на 500.000 тенге на контенте\n\n🎁Я хочу подарить тебе свой воркбук: «5 главных трендов контента в 2023 году», для этого перейди в канал по ссылке ниже и подпишись\n\nА так же включи уведомления, чтобы не пропустить полезные посты и бесплатные уроки👇🏻\n\nhttps://' + CHANNEL)
    else:
        await update.message.reply_text("Ты уже есть в боте")


async def get_message(update: Update, context: CallbackContext):
    # print((update.message.photo[0].to_json(),))
    print((update.message.text,))
    user = session.query(User).filter_by(user_id=update.message.chat_id).first()
    print(update.message.message_id)
    if update.message.message_id - 2 == user.last_message:
        user.event = update.message.text
        session.commit()
        markup = ReplyKeyboardMarkup(
            keyboard=[[KeyboardButton(text="📲отправить контакт", request_contact=True)]],
            resize_keyboard=True)
        await update.message.reply_text("Чтобы я связался с вами, нажмите, пожалуйста, на кнопку в меню и отправьте свой контакт😌", reply_markup=markup)
    elif update.message.text == "🗣обо мне":
        await update.message.reply_text(
            'Меня зовут Артем, я с командой IT-разработчиков создаю Телеграм-сервисы любой сложности'
            '\n\n⚡️Среди наших известных клиентов и партнёров за 2022 год:\n<a href="https://instagram.com/khodzhaev_azam">Азам Ходжаев</a> (2.8М)\n<a href="https://instagram.com/s_romanovich">Сергей Романович</a> (1.3М)\n<a href="https://instagram.com/raze_iam">Андрей Белоусов</a> (230к)\n<a href="https://instagram.com/ivanenko.bz">Алексей Иваненко</a> (100к)\n<a href="https://instagram.com/faldin.a.e">Команда SAYYES</a> (15к)'
            '\n\nМой подход – снимать всю головную боль с клиента, чтобы он успевал заниматься своими делами, в то время как мы с командой делаем задачу без лишних вопросов.'
            '\n\nТакже одна из моих особенностей – помогать клиенту с точки зрения маркетинга и логики реализовывать проект.\n\nЯ работаю не просто по какому-то ТЗ, я через призму своего опыта создаю проект под ключ.',
            parse_mode="HTML")
        await update.message.reply_text(
            'Для связи со мной пишите в личные сообщения (@guricheev), либо оставьте заявку прямо в боте\n\nЯ бесплатно проконсультирую насчет вашего вашего проекта и подскажу, что можно реализовать😌')
    elif update.message.text == "🌟кейсы":
        markup = InlineKeyboardMarkup(
            inline_keyboard=[[InlineKeyboardButton(text='читать↗️️️', callback_data="читать↗")],
                             [InlineKeyboardButton(text='далее👉🏻️️️', callback_data="1")]],
            resize_keyboard=True)
        await update.message.reply_photo(photo=photos[1]["photo"],
                                         caption='⚡️Реферальная воронка, которая увеличила лидов с 9.000 до 29.000'
                                                 '\n\nКлиент: <a href="https://instagram.com/khodzhaev_azam">Азам Ходжаев</a>'
                                                 '\n\nНиша: инфобизнес, NFT\n\nМы с командой создали бота с геймификацией, который увеличивал количество людей в воронке за счёт рекомендательного маркетинга',
                                         parse_mode="HTML", reply_markup=markup)
    elif update.message.text == "✍🏻обсудить проект":
        await update.message.reply_text(
            'Если вы хотите поработать со мной, то можете оставить заявку прямо в боте\n\nМы работаем с проектами любой сложности:\n\n– ОТ обычных ботов под запуски и сервисов под микро-задачи\n\n– ДО WebApp-ботов с интеграцией к сайту и сервисов с подписками, реферальными воронками и т.д.\n\nОсновные ниши наших проектов: инфобизнес, криптовалюта\n\nТакже можем реализовать проект в любой другой нише, если нам будет это интересно')
        markup = InlineKeyboardMarkup(
            inline_keyboard=[[InlineKeyboardButton(text="🗣хочу обсудить", callback_data="🗣хочу обсудить")],
                             [InlineKeyboardButton(text='💢отмена', callback_data="💢отмена")]],
            resize_keyboard=True)
        await update.message.reply_text(
            "⚡️Перед тем, как начать работу, я провожу глубокий брифинг и узнаю запрос, а затем бесплатно показываю, как это можно реализовать\n\nЧтобы обсудить со мной вашу идею проекта, нажмите на кнопку ниже ⬇️",
            reply_markup=markup)


async def answer(update: Update, context: CallbackContext):
    await update.callback_query.message.delete()
    if update.callback_query.data in map(str, range(0, 5)):
        call_data = int(update.callback_query.data) + 1
        markup = InlineKeyboardMarkup(
            inline_keyboard=[[InlineKeyboardButton(text='читать↗️️️', callback_data="читать↗")],
                             [InlineKeyboardButton(text='далее👉🏻️️️',
                                                   callback_data=f"{call_data % 5}")]],
            resize_keyboard=True)
        await update.callback_query.message.reply_photo(photo=photos[call_data]["photo"],
                                                        caption=photos[call_data]["text"],
                                                        parse_mode="HTML", reply_markup=markup)
    elif update.callback_query.data == "💢отмена":
        await context.bot.delete_message(message_id=update.callback_query.message.message_id - 1, chat_id=update.callback_query.message.chat_id)
    elif update.callback_query.data == "🗣хочу обсудить":
        await context.bot.delete_message(message_id=update.callback_query.message.message_id - 1, chat_id=update.callback_query.message.chat_id)
        await update.callback_query.message.reply_text(
            'Опишите, пожалуйста, свой запрос и тезисно идею проекта. Что хотите реализовать?', reply_markup=ReplyKeyboardRemove())
        user = session.query(User).filter_by(user_id=update.callback_query.message.chat_id).first()
        user.last_message = update.callback_query.message.message_id
        session.commit()
    elif update.callback_query.data == "channel":
        if (await context.bot.get_chat_member(chat_id="@" + CHANNEL.split("/")[1],
                                              user_id=update.callback_query.message.chat_id)).status != "left":
            await context.bot.send_document(chat_id=update.callback_query.message.chat_id, document=FILE, caption="Вижу твою подписку на канал!\nДержи обещанный бонус❤️")
        else:
            await send_markup_with_text(update.callback_query.message, subs_markup, '🥳Добро пожаловать на мой бесплатный интенсив, где мы разберем пошаговый план выхода на 500.000 тенге на контенте\n\n🎁Я хочу подарить тебе свой воркбук: «5 главных трендов контента в 2023 году», для этого перейди в канал по ссылке ниже и подпишись\n\nА так же включи уведомления, чтобы не пропустить полезные посты и бесплатные уроки👇🏻\n\nhttps://' + CHANNEL)


async def get_contact(update: Update, context: CallbackContext):
    user = session.query(User).filter_by(user_id=update.message.chat_id).first()
    if user != None:
        user.phone = update.message.contact.phone_number
        session.commit()
        markup = ReplyKeyboardMarkup(
            keyboard=[[KeyboardButton(text="🗣обо мне"), KeyboardButton(text="🌟кейсы")],
                      [KeyboardButton(text='✍🏻обсудить проект')]],
            resize_keyboard=True)
        await update.message.reply_text("Отлично! Я свяжусь с ваши в ближайшие сутки для обсуждения проекта😌", reply_markup=markup)

        await context.bot.send_message(chat_id=1064214243, text=f"Клиент!\n {update.message.contact.user_id}")

async def get_document(update: Update, context: CallbackContext):
    print(update.to_json())


if __name__ == "__main__":
    try:
        # Base.metadata.create_all(engine)

        application = ApplicationBuilder().token(TOKEN).build()

        start_handler = CommandHandler('start', start_message)
        message_handler = MessageHandler(filters.TEXT, get_message)
        document_handler = MessageHandler(filters.ALL, get_document)
        contact_handler = MessageHandler(filters.CONTACT, get_contact)
        button_handler = CallbackQueryHandler(answer)
        application.add_handlers([start_handler, message_handler, button_handler, contact_handler, document_handler])

        application.run_polling()
    except Exception as ex:
        logging.error(traceback.format_exc())
        print(ex)