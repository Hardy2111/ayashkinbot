from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import declarative_base, sessionmaker

Base = declarative_base()
engine = create_engine('sqlite:///Data.db', echo=True)
Session = sessionmaker(bind=engine)
session = Session()

class User(Base):
    __tablename__ = 'users'
    user_id = Column(Integer, primary_key=True)
    user_name = Column(String)
    last_message = Column(Integer)
    phone = Column(Integer)
    event = Column(String)

    def __str__(self):
        return self.user_id

if __name__ == "__main__":
    Base.metadata.create_all(engine)