FROM python:3.8
WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY ./main.py ./main.py
COPY ./create_db.py ./create_db.py
